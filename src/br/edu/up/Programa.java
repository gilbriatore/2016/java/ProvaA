package br.edu.up;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Programa {

	public static void main(String[] args) throws IOException {

		File arquivo = new File("c:/clientes.csv");
		//Scanner leitor = new Scanner(arquivo);
		
		FileReader fr = new FileReader(arquivo);
		BufferedReader br = new BufferedReader(fr);

		List<Cliente> lista = new ArrayList<Cliente>();
		String linha = br.readLine();

		while (linha != null) {

			//String linha = leitor.nextLine();
			String[] dados = linha.split(";");

			Cliente cliente = new Cliente();
			cliente.setCpf(dados[0]);
			cliente.setNome(dados[1]);
			cliente.setCodigo(dados[2]);
			
			String valor = dados[3].replace(",", ".");
			cliente.setCredito(Double.parseDouble(valor));

			lista.add(cliente);
			
			linha = br.readLine();
		}
		
		//leitor.close();

		int a = 0, b = 0, c = 0, d = 0, e = 0;

		for (Cliente cliente : lista) {

			if (cliente.getCredito() > 4000) {

				if (cliente.getNome().startsWith("A")) {
					a++;
				}

				if (cliente.getNome().startsWith("B")) {
					b++;
				}

				if (cliente.getNome().startsWith("C")) {
					c++;
				}

				if (cliente.getNome().startsWith("D")) {
					d++;
				}

				if (cliente.getNome().startsWith("E")) {
					e++;
				}
			}
		}
		
		System.out.println("A: " + a);
		System.out.println("B: " + b);
		System.out.println("C: " + c);
		System.out.println("D: " + d);
		System.out.println("E: " + e);

	}
}